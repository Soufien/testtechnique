

## Test technique iOS

----------
## Prerequistes

 - Xcode version : 8.3.2 (8E2002)
 - Swift compiler version : Apple Swift version 3.1 (swiftlang-802.0.53 clang-802.0.42)
 - Cocoapods version : 1.2.1

----------
## Getting Started

1. `git clone https://Soufien@bitbucket.org/Soufien/testtechnique.git` 
1. `cd testtechnique`
1. `pod install`
1. `open TestTechnique.xcworkspace`
