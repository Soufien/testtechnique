//
//  EpisodeLIstTableViewController.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 03/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import UIKit

class EpisodeLIstTableViewController: UITableViewController {
    
    // Liste of episodes : Data source of the tableview
    var episodes: Array<Episode>?
    var currentIndexPathForFoldedCell: IndexPath?
    
    weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Prepare UI staff
        prepareViews()
        
        // Load episodes Liste
        loadEpisodesList()
        
        //
        NotificationCenter.default.addObserver(self, selector: #selector(EpisodeLIstTableViewController.updateTableView(notification:)), name: .AnimateCellView, object: nil)

    }
    
    // MARK: - Helpsers
    
    func prepareViews(){
        
        // Controller title
        self.title = "Stars Wars"
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 150
        
        //
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        tableView.backgroundView = activityIndicatorView
        self.activityIndicatorView = activityIndicatorView
        
        
    }
    
    func loadEpisodesList(){
        activityIndicatorView.startAnimating()
        RestApiManager.sharedInstance.getEpisodes { (episodes, error) in
            if (error == nil){
                self.episodes = episodes
                self.episodes?.sort { $0.year!.compare($1.year!, options: .numeric) == .orderedAscending }
                self.tableView.reloadData()
            }
            self.activityIndicatorView.stopAnimating()
        }
    }

    //
    func updateTableView(notification: NSNotification){
        
        if let userInfo = notification.userInfo {
            if let indexPath = userInfo["indexPath"] as? IndexPath {
                currentIndexPathForFoldedCell = indexPath
            }
        }
        
        //
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        self.loadViewIfNeeded()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return episodes?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifierForEpisodeCell", for: indexPath) as! EpisodeCustomCellView

        //
        cell.currentIndexPath = indexPath
        cell.setupWithEpisode(episode: (episodes?[indexPath.row])!)
        cell.updateFOldingState(index: currentIndexPathForFoldedCell)
        
        //
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.performSegue(withIdentifier: "toEpisodeDetailViewController", sender: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //
        cell.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            cell.alpha = 1.0
        }, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toEpisodeDetailViewController"{
            if let episodeDetailViewController = segue.destination as? EpisodeDetailViewController{
                episodeDetailViewController.currentEpisode = episodes?[(self.tableView.indexPathForSelectedRow?.row)!]
            }
        }
    }
    

}
