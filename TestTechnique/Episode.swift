//
//  Episode.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 01/06/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Episode: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let year = "year"
    static let title = "title"
    static let image = "image"
    static let intro = "intro"
    static let text = "text"
  }

  // MARK: Properties
  public var year: String?
  public var title: String?
  public var image: String?
  public var intro: String?
  public var text: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    year <- map[SerializationKeys.year]
    title <- map[SerializationKeys.title]
    image <- map[SerializationKeys.image]
    intro <- map[SerializationKeys.intro]
    text <- map[SerializationKeys.text]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = year { dictionary[SerializationKeys.year] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = intro { dictionary[SerializationKeys.intro] = value }
    if let value = text { dictionary[SerializationKeys.text] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.year = aDecoder.decodeObject(forKey: SerializationKeys.year) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.intro = aDecoder.decodeObject(forKey: SerializationKeys.intro) as? String
    self.text = aDecoder.decodeObject(forKey: SerializationKeys.text) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(year, forKey: SerializationKeys.year)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(intro, forKey: SerializationKeys.intro)
    aCoder.encode(text, forKey: SerializationKeys.text)
  }

}
