//
//  RestApiManager.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 03/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


typealias EpisodesListCallback = (_ episodes: Array<Episode>?, _ error: NSError?) -> ()

class RestApiManager: NSObject {
    
    // The shared instance for the RestApiMAnager
    static let sharedInstance = RestApiManager();
    
    // The base URL for the rest API
    let baseURL = "http://www.mocky.io/v2/5915cf73100000470575966f";
    
    
    // Get episode list
    func getEpisodes(callback: @escaping EpisodesListCallback){
        
        Alamofire.request(baseURL).responseJSON { response in
            
            if let JSON = response.result.value {

                let episodes = Mapper<Episode>().mapArray(JSONObject: JSON)
                callback(episodes, nil)
                
            }
        }

    }
    
    
}
