//
//  EpisodeCustomCellView.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 03/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import UIKit

class EpisodeCustomCellView: UITableViewCell {

    //
    @IBOutlet var cellCustomAccessoryIndicator: UIButton?
    @IBOutlet var customContentView: UIView?

    //
    @IBOutlet var episodeCover: UIImageView?
    @IBOutlet var episodeTitle: UILabel?
    @IBOutlet var episodeIntro: UILabel?
    @IBOutlet var episodeYear: UILabel?
    
    
    var isCellFolded: Bool = true
    
    var currentIndexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //
        initViews()
    }
    
    //
    func initViews(){
        //
        self.customContentView?.layer.borderWidth = 1.0;
        self.customContentView?.layer.cornerRadius = 17.5;
        self.customContentView?.layer.borderColor = UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.00).cgColor;
        self.customContentView?.layer.masksToBounds = true;
        
        self.episodeCover?.layer.cornerRadius = 15.0;
        self.episodeCover?.layer.masksToBounds = true;
        self.cellCustomAccessoryIndicator?.layer.masksToBounds = true;
        
        //
        self.episodeIntro?.numberOfLines = 3
    }

    //
    func setupWithEpisode(episode:Episode){
        //
        if let _ = episode.image{
            self.episodeCover?.setImageWithFadeFromURL(url: URL(string:(episode.image)!)!)
        }
        
        //
        self.episodeTitle?.text = episode.title
        self.episodeIntro?.text = episode.intro
        self.episodeYear?.text = episode.year
        
    }
    
    // Add some obeservers
    func registerObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(EpisodeCustomCellView.receivedFoldCellViewAction(notification:)), name: .FoldCellView, object: nil)
        
    }
    
    //
    func unregisterObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    //
    func receivedFoldCellViewAction(notification: NSNotification){
        if !isCellFolded{
            foldUnfoldCellView()
            unregisterObserver()
        }
        
    }
    
    @IBAction func foldCell(_ sender:Any?){
        foldUnfoldCellView()
    }
    
    func updateFOldingState(index:IndexPath?){
        if currentIndexPath != index{
            self.cellCustomAccessoryIndicator?.transform = CGAffineTransform(rotationAngle: 0.0);
            self.episodeIntro?.numberOfLines = 3
        }else{
            self.cellCustomAccessoryIndicator?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2));
            self.episodeIntro?.numberOfLines = 0
        }
        
        //
        self.layoutIfNeeded()
    }
    
    //
    func foldUnfoldCellView(){
        
        //
        UIView.animate(withDuration: 0.3) {
            self.cellCustomAccessoryIndicator?.transform = CGAffineTransform(rotationAngle: self.isCellFolded ? CGFloat(Double.pi/2):0.0);
            self.episodeIntro?.numberOfLines = self.isCellFolded ? 0 : 3
            self.layoutIfNeeded()
        }
        
        //
        let animation: CATransition = CATransition()
        animation.duration = 0.6
        animation.type = self.isCellFolded ? kCATransitionFromBottom : kCATransitionFromTop
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.episodeIntro?.layer.add(animation, forKey: "changeTextTransition")
        
        //
        if let _ = currentIndexPath{
            NotificationCenter.default.post(name: .AnimateCellView, object: nil, userInfo: isCellFolded ? ["indexPath":currentIndexPath!] : nil)
        }
        
        //
        if isCellFolded{
            NotificationCenter.default.post(name: .FoldCellView, object: nil)
            registerObserver()
        }

        //
        isCellFolded = !isCellFolded
    }
    
    //
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // We dont need the selectedBackgroundView
        self.selectedBackgroundView = UIView(frame: .zero)
        
        // Use the customContentView backgroundColor for selection
        self.customContentView?.backgroundColor = selected ? UIColor(red:0.67, green:0.67, blue:0.67, alpha:1.00):UIColor.white
        
    }

}
