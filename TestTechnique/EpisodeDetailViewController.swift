//
//  EpisodeDetailViewController.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 03/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import UIKit
import SDWebImage

class EpisodeDetailViewController: UIViewController {

    var currentEpisode: Episode?
    
    @IBOutlet var episodeCover: UIImageView?
    @IBOutlet var episodeInfo: UITextView?
    @IBOutlet var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Prepare UI staff
        prepareViews()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Helpsers
    func prepareViews(){
        
        // Controller title
        self.title = currentEpisode?.title
        
        // Load cover image for the current episode
        episodeCover?.setImageWithFadeFromURL(url: URL(string:(currentEpisode?.image)!)!)
        
        let episodeInfoAttributedString = NSMutableAttributedString()
        
        // Text views
        let introAttributedParams = [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 12.0) ]
        let introAttributedString = NSAttributedString(string: (currentEpisode?.intro)!, attributes: introAttributedParams)
        
        let episodeTextAttributedParams = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 15.0) ]
        let episodeTextAttributedString = NSAttributedString(string: (currentEpisode?.intro)!, attributes: episodeTextAttributedParams)

        episodeInfoAttributedString.append(introAttributedString)
        episodeInfoAttributedString.append(NSAttributedString(string: "\n\n"))
        episodeInfoAttributedString.append(episodeTextAttributedString)
        
        episodeInfo?.attributedText = episodeInfoAttributedString
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
