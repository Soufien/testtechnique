//
//  UIImageView+Extensions.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 03/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    
    // Extension added to make showing loaded image from the api with fade animation
    public func setImageWithFadeFromURL(url: URL, placeholderImage placeholder: UIImage? = nil, animationDuration: Double = 0.3) {
        
        self.sd_setImage(with: url) { (image, error, cacheType, url) in
            
            if error != nil {
                print("Error loading Image from URL: \(String(describing: url))\n \(String(describing: error?.localizedDescription))")
            }
            
            self.alpha = 0
            self.image = image
            UIView.transition(with: self, duration: (cacheType == .none ? animationDuration : 0), options: .transitionCrossDissolve, animations: { () -> Void in
                self.alpha = 1
            }, completion: nil)
            
        }
            
    }

}
