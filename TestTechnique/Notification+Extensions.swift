//
//  Notification+Extensions.swift
//  TestTechnique
//
//  Created by Soufien Hidouri on 04/06/2017.
//  Copyright © 2017 Soufien Hidouri. All rights reserved.
//

import Foundation

// Definition:
extension Notification.Name {
    
    //
    static let AnimateCellView = Notification.Name("AnimateCellView")
    
    //
    static let FoldCellView = Notification.Name("FoldCellView")
    
}
